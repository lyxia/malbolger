// Interpreter for Malbolge
// Ported from the C version by Ben Olmstead

use std::env;
use std::io::{self, BufReader, BufWriter, Read, Write};
use std::fs::File;

// Machine words are ten trits wide.
// Memory space is exactly 59049 words long.
const MEM_SIZE: usize = 59049;

#[allow(non_upper_case_globals)]
const xlat1: &'static [u8; 94] = b"\
    +b(29e*j1VMEKLyC})8&m#~W>qxdRp0wkrUo[D7,XTcA\"lI\
    .v%{gJh4G\\-=O@5`_3i<?Z';FNQuY]szf$!BS/|t:Pn6^Ha";

#[allow(non_upper_case_globals)]
const xlat2: &'static [u8; 94] = b"\
    5z]&gqtyfr$(we4{WP)H-Zn,[%\\3dL+Q;>U!pJS72FhOA1C\
    B6v^=I_0/8|jsb9m<.TVac`uY*MK'X~xDl}REokN:#?G\"i@";

macro_rules! xlat1 { ( $x: expr, $c: expr ) => { xlat1[($x - 33 + $c) % 94] } }

macro_rules! xlat2 { ( $x: expr ) => { xlat2[$x - 33] } }

fn main() {
    let mut mem = {
        if env::args().count() != 2 {
            panic!("invalid command line")
        };
        let p = env::args().nth(1).unwrap();
        read_from(p)
    };
    exec(&mut mem[..]);
}

// When the interpreter loads the program, it ignores all whitespace.  If
// it encounters anything that is not one of an instruction ("ji*</pvo", o: nop)
// and is not whitespace,
// it will give an error, otherwise it loads the file, one non-
// whitespace character per cell, into memory.  Cells which are not
// initialized are set by performing op on the previous two cells
// repetitively.
fn read_from(p: String) -> Vec<usize> {
    let f = File::open(p).unwrap();
    let mut mem = vec![0; MEM_SIZE];
    let mut i = 0;
    for c in f.bytes().map(Result::unwrap) {
        if (c as char).is_whitespace() { continue };
        if i == MEM_SIZE { panic!("input file too long") };
        if 32 < c && c < 127 {
            let k = xlat1!(c as usize, i);
            if !b"ji*</pvo".contains(&k) {
                panic!(
                    "Invalid character at {}: {} (maps to {})",
                    i, c as char, k);
            }
            mem[i] = c as usize;
            i += 1;
        }
        else {
            panic!("Invalid character at {}: {} (unprintable)", i, c);
        }
    }
    for i in std::cmp::max(2, i)..MEM_SIZE {
        mem[i] = op(mem[i-1], mem[i-2]);
    }
    mem
}

// The three registers are A, C, and D.  A is the accumulator, used for
// data manipulation.  A is implicitly set to the value written by all
// write operations on memory.  (Standard I/O, a distinctly non-chip-level
// feature, is done directly with the A register.)
// 
// C is the code pointer.  It is automatically incremented after each
// instruction, and points to the instruction being executed.
// 
// D is the data pointer.  It, too, is automatically incremented after each
// instruction, but the location it points to is used for the data
// manipulation commands.
fn exec(mem: &mut [usize]) {
    let mut a = 0usize;
    let mut c = 0usize;
    let mut d = 0usize;
    let mut stdout = BufWriter::new(io::stdout());
    let mut stdin = BufReader::new(io::stdin()).bytes()
        .map(Result::unwrap).map(|x| { x as usize });
    loop {
        if mem[c] < 33 || 126 < mem[c] { continue };
        // A layer of obfuscation in fetching instructions
        match xlat1!(mem[c], c) as char {
            'j' => d = mem[d],
            'i' => c = mem[d],
            '*' => { // Trinary rotate
                a = mem[d] / 3 + mem[d] % 3 * 19683;
                mem[d] = a;
            },
            'p' => {
// performs a tritwise "op" on the value pointed to by D with the
// contents of A.
// The op (don't look for pattern, it's not there) is:

//           | A trit:
//   ________|_0__1__2_
//         0 | 1  0  0
//     *D  1 | 1  0  2
//    trit 2 | 2  2  1
                a = op(a, mem[d]);
                mem[d] = a;
            },
            '<' => stdout.write_all(&[a as u8]).unwrap(),
            '/' => a = stdin.next().unwrap_or(MEM_SIZE-1),
            'v' => return,
            _ => {}
        }
        mem[c] = xlat2!(mem[c]) as usize;
        c = (c + 1) % MEM_SIZE;
        d = (d + 1) % MEM_SIZE;
    }
}

fn op(x: usize, y: usize) -> usize {
    #[allow(non_upper_case_globals)]
    static o: [[usize; 9]; 9] = [
        [ 4, 3, 3, 1, 0, 0, 1, 0, 0 ],
        [ 4, 3, 5, 1, 0, 2, 1, 0, 2 ],
        [ 5, 5, 4, 2, 2, 1, 2, 2, 1 ],
        [ 4, 3, 3, 1, 0, 0, 7, 6, 6 ],
        [ 4, 3, 5, 1, 0, 2, 7, 6, 8 ],
        [ 5, 5, 4, 2, 2, 1, 8, 8, 7 ],
        [ 7, 6, 6, 7, 6, 6, 4, 3, 3 ],
        [ 7, 6, 8, 7, 6, 8, 4, 3, 5 ],
        [ 8, 8, 7, 8, 8, 7, 5, 5, 4 ],
    ];
    let mut i = 0;
    let mut t = 1;
    for _ in 0..5 {
        i += o[(y / t) % 9][(x / t) % 9] * t;
        t *= 9;
    }
    i
}

